// This was used to determine what colour sets from table 2 and 3
// in this article https://www.sciencedirect.com/science/article/pii/S0195669812001369
// were found.

//selects the implemntaion of Graph structure. As of now there is only one.
#include <basic_impl.hpp>

//#include <impl/basic/include.hpp>
#include <io.hpp>
#include <invariants.hpp>
#include <mrozek.hpp>
//#include <util/cxxopts.hpp>

using namespace ba_graph;

#include <string>
#include <sstream>
#include <fstream>
#include <memory>

std::tuple<std::string, std::string, std::vector<Number>> parse_graphs_file_line(std::string line) {
    auto spacePos = line.find(" ");
    std::string cbaStr = line.substr(0, spacePos);
    std::string lineRemainder = line.substr(spacePos + 1);
    spacePos = lineRemainder.find(" ");
    std::string graph6 = lineRemainder.substr(0, spacePos);
    lineRemainder = lineRemainder.substr(spacePos + 1);
    auto bracketPos = lineRemainder.find("]");
    std::string numbers = lineRemainder.substr(1, bracketPos);
    std::stringstream numbers_ss(numbers);
    std::string number;
    std::vector<Number> order;
    while(getline(numbers_ss, number, ' ')) { 
         order.push_back(Number(std::stoi(number)));
    }

    return {cbaStr, graph6, order};
}

ColourSet colourTypeToColourVectors(ColourVector cv) {
    std::vector<std::vector<std::vector<int>>> transformationMaps = {
        {{0,1}, {1,0}},
        {{0,2}, {2,0}},
        {{1,2}, {2,1}},
        {{0,1}, {1,2}, {2,0}},
        {{0,2}, {2,1}, {1,0}}
    };

    ColourSet cs;
    cs.insert(cv);
    for (auto map : transformationMaps) {
        ColourVector newVector = cv;
        for (int i = 0; i < newVector.size(); i++) {
            for (auto mapping: map) {
                if (mapping[0] == cv.at(i))
                    newVector.at(i) = mapping[1];
            }
        }
        cs.insert(newVector);
    }

    return cs;
}

ColourSet colourSetToFullColourSet(ColourSet cs) {
    ColourSet fullColourSet;
    for (ColourVector cv: cs) {
        ColourSet part = colourTypeToColourVectors(cv);
        fullColourSet.insert(part.begin(), part.end());
    }
    return fullColourSet;
}

int main() {
    std::vector<ColourVector> allVectors = {
        {0,1,2,2,1,0},
        {0,1,2,1,2,0},
        {0,1,1,2,2,0},
        {0,1,2,2,0,1},
        {0,1,2,1,0,2},
        {0,1,1,2,0,2},
        {0,1,2,0,2,1},
        {0,1,2,0,1,2},
        {0,1,1,0,2,2},
        {0,1,0,2,2,1},
        {0,1,0,2,1,2},
        {0,1,0,1,2,2},
        {0,0,1,2,2,1},
        {0,0,1,2,1,2},
        {0,0,1,1,2,2},
        {0,0,0,0,1,1},
        {0,0,0,1,0,1},
        {0,0,0,1,1,0},
        {0,0,1,0,0,1},
        {0,0,1,0,1,0},
        {0,0,1,1,0,0},
        {0,1,0,0,0,1},
        {0,1,0,0,1,0},
        {0,1,0,1,0,0},
        {0,1,1,0,0,0},
        {0,1,1,1,1,0},
        {0,1,1,1,0,1},
        {0,1,1,0,1,1},
        {0,1,0,1,1,1},
        {0,0,1,1,1,1},
        {0,0,0,0,0,0},
    };
    std::vector<int> sets = {
        1112539137, 219, 995328,
        1782, 983094, 983277, 1186463953, 259394321, 1186467169,
        15275, 986027, 1003880, 1003955, 1005470,
        107940770, 108429655, 108429708, 108431226, 108441953,
        108442042, 108454116, 108456866, 259391628,
        259394506, 259394506, 259415138, 1112539354, 1112542108, 1112554410, 1113522412, 1113525162, 1113543090, 1113545460,
        1113545730, 1186479009, 1186490916, 112984273, 127372309,
        7846403, 113014676, 251396177, 112987505, 127384611, 
        258968226, 117291537, 16744821
    };

    std::unordered_map<std::string, std::vector<std::string>> canonsWithGraphsMap;
    std::ifstream infile("../out/k6_graphs_pdc_do18");
    std::string line;
    while (std::getline(infile, line)) {
        auto parsed = parse_graphs_file_line(line);
        auto i = canonsWithGraphsMap.find(std::get<0>(parsed));
        if (i != canonsWithGraphsMap.end()) {
            i->second.push_back(std::get<1>(parsed));
        } else {
            canonsWithGraphsMap.insert({std::get<0>(parsed), {std::get<1>(parsed)}});
        }
    }

    std::ofstream ofs;
    ofs.open("../out/6decompositionWhatIFound", std::ios_base::trunc);
    for (auto set : sets) {
        ColourSet cs;
        for (int i = 0; i < 32; i++) {
            unsigned mask = 1 << i;
            if (set & mask) {
                cs.insert(allVectors.at(i));
            }
        }
        // std::cout << cs << "\n";
        // std::cout << colourSetToFullColourSet(cs) << "\n";

        std::vector<Number> terminals = {Number(0), Number(1), Number(2), Number(3), Number(4), Number(5)};
        auto res = get_canonical_colour_set(colourSetToFullColourSet(cs), terminals);
        std::cout << std::get<1>(res) << "\n";

        ofs << set << " ";

        auto i = canonsWithGraphsMap.find(print_nice_cba(std::get<1>(res)));
        if (i != canonsWithGraphsMap.end()) {
            ofs << i->second << "\n";
        } else {
            ofs << "NOPE\n";
        }
    }
    ofs.close();
}