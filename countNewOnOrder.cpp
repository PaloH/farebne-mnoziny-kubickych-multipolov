// This was used to determine how many new colour kinds were found on multipoles with particular order.
// We are parsing our output graph file from collect_canonical_colour_sets.cpp. It contains canons with
// minimal multipoles, terminal order and multipole order.

//selects the implemntaion of Graph structure. As of now there is only one.
#include <basic_impl.hpp>

//#include <impl/basic/include.hpp>
#include <io.hpp>
#include <invariants.hpp>
#include <mrozek.hpp>
//#include <util/cxxopts.hpp>

using namespace ba_graph;

#include <string>
#include <sstream>
#include <fstream>
#include <memory>

std::tuple<std::string, int> parse_graphs_file_line(std::string line) {
    auto spacePos = line.find(" ");
    std::string cbaStr = line.substr(0, spacePos);
    std::string lineRemainder = line.substr(spacePos + 1);
    spacePos = lineRemainder.find(" ");
    lineRemainder = lineRemainder.substr(spacePos + 1);
    auto bracketPos = lineRemainder.find("]");
    std::string graphOrderStr = lineRemainder.substr(bracketPos + 2);

    return {cbaStr, std::stoi(graphOrderStr)};
}

int main(int argc, char** argv) {
    if (argc != 2) {
        std::cout << "ERROR: too few arguments" << "\n";
        std::cout << "args format: <path to file with graphs>" << "\n";
        return 1;
    }
    std::vector<int> newCounts(20, 0);
    std::ifstream infile(argv[1]);
    std::string previousCBA;
    std::string line;
    while (std::getline(infile, line)) {
        auto parsed = parse_graphs_file_line(line);
        if (std::get<0>(parsed) == previousCBA) continue;
        previousCBA = std::get<0>(parsed);
        newCounts.at(std::get<1>(parsed))++;
    }

    for (int i = 0; i < newCounts.size(); i++) {
        if (newCounts.at(i) != 0)
            std::cout << "order: " << i << "   found new canons: " << newCounts.at(i) << "\n";
    }
}
