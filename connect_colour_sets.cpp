//selects the implemntaion of Graph structure. As of now there is only one.
#include <basic_impl.hpp>

//#include <impl/basic/include.hpp>
#include <io.hpp>
#include <invariants.hpp>
#include <mrozek.hpp>
//#include <util/cxxopts.hpp>

using namespace ba_graph;

#include <string>
#include <sstream>
#include <fstream>
#include <memory>

struct cba_hash {
    std::size_t operator () (const ColouringBitArray &cba) const {
        std::stringstream binary;
        for (auto i = ColouringBitArray::Index(0, 0); i < cba.size(); i++) {
            if (cba.get(i)) binary << 1;
            else binary << 0;
        }
        return std::hash<std::string>{}(binary.str()); 
    }
};

// source: https://www.geeksforgeeks.org/make-combinations-size-k/
void makeCombiUtil(std::vector<std::vector<int>>& ans, std::vector<int>& tmp, int n, int left, int k) { 
    // Pushing this vector to a vector of vector 
    if (k == 0) { 
        ans.push_back(tmp); 
        return; 
    } 
  
    // i iterates from left to n. First time 
    // left will be 1 
    for (int i = left; i < n; ++i) 
    { 
        tmp.push_back(i); 
        makeCombiUtil(ans, tmp, n, i + 1, k - 1); 
  
        // Popping out last inserted element 
        // from the vector 
        tmp.pop_back(); 
    } 
} 
  
// Prints all combinations of size k of numbers 
// from 0 to n. 
std::vector<std::vector<int> > makeCombi(int n, int k) 
{ 
    std::vector<std::vector<int> > ans; 
    std::vector<int> tmp; 
    makeCombiUtil(ans, tmp, n, 0, k); 
    return ans; 
}

ColouringBitArray parse_cba(std::string cba_str) {
    ColouringBitArray cba(cba_str.size(), false);
    for(int i = 0; i < cba_str.size(); i++) {
        if (cba_str[i] == '1') cba.set(ColouringBitArray::Index::to_index(i), true);
    }
    return cba;
}

std::tuple<ColouringBitArray, std::string, std::vector<Number>> parse_graphs_file_line(std::string line) {
    auto spacePos = line.find(" ");
    std::string cbaStr = line.substr(0, spacePos);
    std::string lineRemainder = line.substr(spacePos + 1);
    spacePos = lineRemainder.find(" ");
    std::string graph6 = lineRemainder.substr(0, spacePos);
    lineRemainder = lineRemainder.substr(spacePos + 1);
    auto bracketPos = lineRemainder.find("]");
    std::string numbers = lineRemainder.substr(1, bracketPos);
    std::stringstream numbers_ss(numbers);
    std::string number;
    std::vector<Number> order;
    while(getline(numbers_ss, number, ' ')) { 
         order.push_back(Number(std::stoi(number)));
    }

    return {parse_cba(cbaStr), graph6, order};
}

std::vector<std::pair<std::shared_ptr<Graph>, std::vector<Number>>> find_matching_graphs(std::string filePath, ColouringBitArray cbaToFind) {
    std::ifstream infile(filePath);
    std::string line;
    std::vector<std::pair<std::shared_ptr<Graph>, std::vector<Number>>> graphs;
    bool foundFirst = false;
    while (std::getline(infile, line)) {
        auto [cba, graph6, order] = parse_graphs_file_line(line);
        if (cba == cbaToFind) {
            foundFirst = true;
            graphs.push_back({std::make_shared<Graph>(read_graph6_line(graph6)), order});
        } else if (foundFirst) break;
    }
    infile.close();

    return graphs;
}

std::vector<ColouringBitArray> connect_canons (
    std::vector<ColouringBitArray>& canons1,
    std::vector<ColouringBitArray>& canons2,
    std::unordered_set<ColouringBitArray, cba_hash>& foundCanons,
    std::vector<std::map<int, int>>& connectionMaps,
    std::string canonFilePath1,
    std::string canonFilePath2,
    std::string outputPath,
    bool is_recursion
) {
    std::string graphsFilePath1 = canonFilePath1;
    std::string graphsFilePath2 = canonFilePath2;

    auto startPos = graphsFilePath1.find("_canons_");
    graphsFilePath1.replace(startPos, 8, "_graphs_");
    startPos = graphsFilePath2.find("_canons_");
    graphsFilePath2.replace(startPos, 8, "_graphs_");

    std::ofstream ofs;
    ofs.open(outputPath, std::ios_base::app);

    std::vector<ColouringBitArray> newCbas;
    for (auto cba1 : canons1) {
        for (auto cba2 : canons2) {
            for (auto connectionMap : connectionMaps) {
                auto newCba = get_canonical_colour_set_of_composite_2(cba1, cba2, connectionMap);
                if (foundCanons.find(newCba) == foundCanons.end()) {
                    foundCanons.insert(newCba);
                    newCbas.push_back(newCba);

                    std::vector<std::pair<std::shared_ptr<Graph>, std::vector<Number>>> graphs1 = find_matching_graphs((is_recursion ? outputPath : graphsFilePath1), cba1);
                    std::vector<std::pair<std::shared_ptr<Graph>, std::vector<Number>>> graphs2 = find_matching_graphs(graphsFilePath2, cba2);
                    if (is_recursion && graphs2.size() == 0) {
                        graphs2 = find_matching_graphs(outputPath, cba2);
                    }

                    if (graphs1.size() == 0) {
                        std::cout << "ERROR: could not find in: " << (is_recursion ? outputPath : graphsFilePath1) << "\ncba:" << cba1 << "\n";
                    }
                    if (graphs1.size() == 0) {
                        std::cout << "ERROR: could not find in both: " << outputPath << " and " << graphsFilePath2 << "\ncba:" << cba2 << "\n";
                    }

                    for (auto g1 : graphs1) {
                        for (auto g2 : graphs2) {
                            std::map<Number, Number> numberConnectionMap;
                            for (auto pair : connectionMap) {
                                numberConnectionMap.insert({g1.second.at(pair.first), g2.second.at(pair.second)});
                            }
                            Graph composite = connect_multipoles(*g1.first, *g2.first, numberConnectionMap);
                            Factory f;
	                        Graph compositeRenumbered(copy_other_factory(composite, f));
                            renumber_consecutive(compositeRenumbered, f);

                            auto [order, cba, time] = get_canonical_colour_set(compositeRenumbered, ColouringMethod::basic);
                            if (newCba != cba) {
                                std::cout << "ERROR: Canons are not equal. Something is wrong." << "\n";
                            } else {
                                ofs << cba << " " << write_sparse6(composite, false) << " " << order << " " << (composite.order() - (int)foundCanons.begin()->size().get_power()) << "\n";
                            }
                        }
                    }
                }
            }
        }
    }
    ofs.close();
    return newCbas;
}

int main(int argc, char** argv) {
    if (argc != 7) {
        std::cout << "ERROR: too few arguments" << "\n";
        std::cout << "note: all paths are always relative and without '/' at the end" << "\n";
        std::cout << "args format: <path to file with first canons> <path to file with second canons> <path to file with already found canons> <output path> <how many free edges connect> <recurse? T/F>" << "\n";
        return 1;
    }
    std::string canonFilePath1 = argv[1];
    std::string canonFilePath2 = argv[2];
    std::ifstream infile(argv[1]);
    std::ifstream infile2(argv[2]);
    std::ifstream infile3(argv[3]);
    int toConnect = std::stoi(argv[5]);
    bool recurse = argv[6] == "T" ? true : false;
    
    std::vector<ColouringBitArray> canons1;
    std::vector<ColouringBitArray> canons2;
    std::unordered_set<ColouringBitArray, cba_hash> foundCanons;
    std::string line;
    while (std::getline(infile, line)) {
        canons1.push_back(parse_cba(line));
    }
    while (std::getline(infile2, line)) {
        canons2.push_back(parse_cba(line));
    }
    while (std::getline(infile3, line)) {
        foundCanons.insert(parse_cba(line));
    }

    std::stringstream ss;
    ss << argv[4] << "/k" << (int)foundCanons.begin()->size().get_power() << "_graphs_connected_" << (int)canons1.begin()->size().get_power() << "_" << (int)canons2.begin()->size().get_power() << "_on_" << toConnect;
    std::string outputPath = ss.str();

    std::vector<std::vector<int>> combinations1 = makeCombi(canons1.at(0).size().get_power(), toConnect);
    std::vector<std::vector<int>> combinations2 = makeCombi(canons2.at(0).size().get_power(), toConnect);

    // create all possible connection maps
    std::vector<std::map<int, int>> connectionMaps;
    for (auto comb1 : combinations1) {
        for (auto comb2 : combinations2) {
            std::vector<int> currPermutation = comb2;
            do {
                std::map<int, int> connectionMap;
                for (int i = 0; i < comb1.size(); i++) {
                    connectionMap.insert({comb1.at(i), currPermutation.at(i)});
                }
                connectionMaps.push_back(connectionMap);
            } while (std::next_permutation(currPermutation.begin(), currPermutation.end()));
        }
    }

    std::vector<ColouringBitArray> allNewCbas = connect_canons(canons1, canons2, foundCanons, connectionMaps, canonFilePath1, canonFilePath2, outputPath, false);

    std::cout << "first round: " << allNewCbas.size() << "\n";

    if (allNewCbas.size() != 0) {
        if (recurse) {
            if (canons1.at(0).size() == foundCanons.begin()->size() && canons2.at(0).size() == foundCanons.begin()->size()) {
                std::vector<ColouringBitArray> newCbas = allNewCbas;
                while (newCbas.size() != 0) {
                    canons2.insert(canons2.end(), newCbas.begin(), newCbas.end());
                    std::vector<ColouringBitArray> newCbasCopy = newCbas;
                    newCbas.clear();
                    newCbas = connect_canons(newCbasCopy, canons2, foundCanons, connectionMaps, canonFilePath1, canonFilePath2, outputPath, true);
                    allNewCbas.insert(allNewCbas.end(), newCbas.begin(), newCbas.end());
                    std::cout << allNewCbas.size() << "\n";
                }
            } else if (canons1.at(0).size() == foundCanons.begin()->size()) {
                std::vector<ColouringBitArray> newCbas = allNewCbas;
                while (newCbas.size() != 0) {
                    std::vector<ColouringBitArray> newCbasCopy = newCbas;
                    newCbas.clear();
                    newCbas = connect_canons(newCbasCopy, canons2, foundCanons, connectionMaps, canonFilePath1, canonFilePath2, outputPath, true);
                    allNewCbas.insert(allNewCbas.end(), newCbas.begin(), newCbas.end());
                }
            } else if (canons2.at(0).size() == foundCanons.begin()->size()) {
                std::vector<ColouringBitArray> newCbas = allNewCbas;
                while (newCbas.size() != 0) {
                    std::vector<ColouringBitArray> newCbasCopy = newCbas;
                    newCbas.clear();
                    newCbas = connect_canons(newCbasCopy, canons1, foundCanons, connectionMaps, canonFilePath1, canonFilePath2, outputPath, true);
                    allNewCbas.insert(allNewCbas.end(), newCbas.begin(), newCbas.end());
                }
            }
        }
        
        std::stringstream filePath;
        filePath << argv[4] << "/k" << (int)foundCanons.begin()->size().get_power() << "_canons_connected_" << (int)canons1.begin()->size().get_power() << "_" << (int)canons2.begin()->size().get_power() << "_on_" << toConnect;
        std::ofstream ofs;
        ofs.open(filePath.str(), std::ios_base::trunc);

        for (auto cba: allNewCbas) {
            ofs << cba << "\n";
        }
        ofs.close();

        std::stringstream filePath2;
        filePath2 << argv[4] << "/k" << (int)foundCanons.begin()->size().get_power() << "_summary_connected_" << (int)canons1.begin()->size().get_power() << "_" << (int)canons2.begin()->size().get_power() << "_on_" << toConnect;
        std::ofstream ofs2;
        ofs2.open(filePath2.str(), std::ios_base::trunc);

        ofs << "Unique canonical colour sets found: " << allNewCbas.size() << "\n\n";
        for (auto cba: allNewCbas) {
            ofs << cba << "\n";
            ofs << colouring_bit_array_to_colour_set(cba) << "\n";
        }
        ofs2.close();
    }

    std::cout << "DONE" << "\n";
}