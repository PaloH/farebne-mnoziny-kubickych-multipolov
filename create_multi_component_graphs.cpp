//selects the implemntaion of Graph structure. As of now there is only one.
#include <basic_impl.hpp>

//#include <impl/basic/include.hpp>
#include <io.hpp>
#include <invariants.hpp>
#include <mrozek.hpp>
//#include <util/cxxopts.hpp>

using namespace ba_graph;

#include <string>
#include <filesystem>
#include <numeric>

struct pair_hash {
    std::size_t operator () (const std::pair<int, int> &p) const {
        std::string s1 = std::to_string(p.first);
        std::string s2 = std::to_string(p.second);
        std::string s = s1 + s2;
        int c = stoi(s);
        return std::hash<int>{}(c); 
    }
};

struct position {
    int curPath; // current position in paths 
    int curLine; // current position in file
};

struct graphProperties {
    int order;
    int girth;
    int k; // number of free ends
};

std::vector<std::string> allPaths;
std::unordered_map<std::pair<int, int>, std::ofstream *, pair_hash> ofstreams;
std::vector<std::vector<int>> permResult;

// generate all possibilities of how to sum up to goal with integers from 1 to goal-1
std::vector<std::vector<int>> make_change(int goal) {
    if (goal == 0) return {};
    std::vector<int> coins(goal - 1);
    std::iota(std::begin(coins), std::end(coins), 1);
    std::vector<std::vector<int>> wallets;
    std::vector<std::vector<int>> newWallets;
    std::vector<std::vector<int>> collected;
    for (int coin : coins) {
        wallets.push_back({coin});
    }
    while (wallets.size() > 0) {
        for (auto wallet : wallets) {
            int s = std::accumulate(wallet.begin(), wallet.end(), 0);
            for (int coin : coins) {
                if (coin >= wallet[wallet.size() - 1]) {
                    if (s + coin < goal) {
                        std::vector<int> newWallet = wallet;
                        newWallet.push_back(coin);
                        newWallets.push_back(newWallet);
                    }
                    else if (s + coin == goal) {
                        std::vector<int> newWallet = wallet;
                        newWallet.push_back(coin);
                        collected.push_back(newWallet);
                    }
                }
            }
        }
        wallets = newWallets;
        newWallets.clear();
    }

    return collected;
}

void rec(std::unordered_map<std::pair<int, int>, int, pair_hash>::iterator bucketIt, std::unordered_map<std::pair<int, int>, int, pair_hash>& buckets, Graph& composite, graphProperties gp);

Graph mergeGraphs(Graph& g1, Graph& g2) {
    Factory f;
	ba_graph::Graph H(copy_other_factory(g1, f));

    Mapping<Number> m;
    for (const auto &rot : g2)
        m.set(rot.n(), addV(H, f).n());

    for (auto ii : g2.list(RP::all(), IP::primary()))
        addE(H, Location(m.get(ii->n1()), m.get(ii->n2())), f);

    return H;
}

void rec2 (
        position curPosition,
        int indexInBucket,
        std::unordered_map<std::pair<int, int>, int, pair_hash>::iterator bucketIt,
        std::unordered_map<std::pair<int, int>, int, pair_hash>& buckets,
        Graph& composite,
        graphProperties gp,
        std::vector<std::string> paths
    ) {

    if (indexInBucket == bucketIt->second) {
        rec(++bucketIt, buckets, composite, gp);
        return;
    }

    bool first = true;
    for (int i = curPosition.curPath; i < paths.size(); i++) {
        int curLine = 0;
        std::ifstream fin(paths.at(i));
        std::string line;

        // if first time we need to skip to the correct position
        if (first) {
            for (int j = 0; j < curPosition.curLine; j++)
                std::getline(fin, line);

            curLine = curPosition.curLine;
            first = false;
        }

        // parse graph properties from file name
        int order = std::stoi(paths.at(i).substr(paths.at(i).find("_o") + 2, paths.at(i).find("_g") - paths.at(i).find("_o") - 2));
        int girth = std::stoi(paths.at(i).substr(paths.at(i).find("_g") + 2, paths.at(i).find("_k") - paths.at(i).find("_g") - 2));
        int k = std::stoi(paths.at(i).substr(paths.at(i).find("_k") + 2));
        // compute graph properties of new composite
        graphProperties newGP = {
            gp.order + order,
            gp.girth == -1 || girth == -1 ? std::max(gp.girth, girth) : std::min(gp.girth, girth),
            gp.k + k,
        };

        while (std::getline(fin, line)) {
            Graph g = read_graph6_line(line); // get graph from line

            Graph newComposite = mergeGraphs(composite, g); // merge graph with composite

            rec2({i, curLine}, indexInBucket + 1, bucketIt, buckets, newComposite, newGP, paths);
            curLine++;
        }
    }
}

void rec(
        std::unordered_map<std::pair<int, int>, int, pair_hash>::iterator bucketIt,
        std::unordered_map<std::pair<int, int>, int, pair_hash>& buckets,
        Graph& composite,
        graphProperties gp
    ) {
    if (bucketIt == buckets.end()) {
        std::pair<int, int> key(gp.order, gp.girth);
        auto i = ofstreams.find(key);
        if (i != ofstreams.end()) {
            *(i->second) << write_sparse6(composite);
        } else {
            std::stringstream filePath;
            filePath << "../out/multipoles_disconnected2/order" << gp.order << "/cubic_o" << gp.order << "_g" << gp.girth << "_k" << gp.k;
            if (!std::filesystem::exists("../out/multipoles_disconnected2/order" + std::to_string(gp.order))) {
                std::filesystem::create_directory("../out/multipoles_disconnected2/order" + std::to_string(gp.order));
            }

            std::ofstream* ofs = new std::ofstream();
            ofs->open(filePath.str(), std::ios_base::app);
            *ofs << write_sparse6(composite);

            ofstreams[key] = ofs;
        }

        return;
    }

    // filter useful paths for particular bucket
    std::vector<std::string> paths;
    for (auto path : allPaths) {
        if (path.find("_k" + std::to_string(bucketIt->first.first)) != std::string::npos &&
            path.size() - path.find("_k" + std::to_string(bucketIt->first.first)) == std::to_string(bucketIt->first.first).size() + 2 &&
            std::stoi(path.substr(path.find("_o") + 2, path.find("_g") - path.find("_o") - 2)) == bucketIt->first.second)
        {
            paths.push_back(path);
        } 
    }

    rec2({0, 0}, 0, bucketIt, buckets, composite, gp, paths);
}

void recursion(int kIndex, std::vector<int> ko, std::map<int, int> availableOrders, std::vector<int> actualPerm) {
    if (actualPerm.size() == ko.size()) {
        // save and return
        permResult.push_back(actualPerm);
        return;
    }
    for (auto orderBucket: availableOrders) {
        if ((kIndex == 0 || ko.at(kIndex - 1) != ko.at(kIndex)) || orderBucket.first >= actualPerm.at(actualPerm.size() - 1)) {
            if ((orderBucket.first + ko.at(kIndex)) & 1) continue; // skip if not same parity (graphs of odd/even order have only odd/even count of free ends)
            std::vector<int> newPerm = actualPerm;
            std::map<int, int> newAvailableOrders(availableOrders);
            newPerm.push_back(orderBucket.first);
            if (newAvailableOrders.at(orderBucket.first) > 1) {
                newAvailableOrders.at(orderBucket.first)--;
            } else {
                newAvailableOrders.erase(orderBucket.first);
            }
            recursion(kIndex + 1, ko, newAvailableOrders, newPerm);
        }
    }
}

int main() {
    int k = 7; // number of wanted free ends
    int order = 19; // wanted order

    for (auto& p: std::filesystem::recursive_directory_iterator("../out/all_multipoles"))
        allPaths.push_back(p.path());

    std::vector<std::vector<int>> kOptions = make_change(k);
    std::vector<std::vector<int>> orderOptions = make_change(order);
    orderOptions.push_back({order});
    // create all usefull order options including zeros and filter useless (too long/short) options
    std::vector<std::vector<int>> orderOptionsWithZero;
    for (auto o : orderOptions) {
        if (o.size() <= k && o.size() >= 2) {
            orderOptionsWithZero.push_back(o);
        }
        if (o.size() < k) {
            for (int i = 1; i <= k - o.size(); i++) {
                std::vector newOption = o;
                for (int j = 0; j < i; j++) newOption.push_back(0);
                orderOptionsWithZero.push_back(newOption);
            }
        }
    }
    std::cout << kOptions << "\n";
    std::cout << orderOptionsWithZero << "\n";

    // generate all feasible pairs of k options and order options
    for (auto kOption : kOptions) {
        for (auto orderOption: orderOptionsWithZero) {
            if (kOption.size() == orderOption.size()) {
                // generate all feasible permutations of these two options
                std::map<int, int> orderBuckets; // mapping o -> count in order option
                for (int i = 0; i <= order; i++) {
                    int count = std::count(orderOption.begin(), orderOption.end(), i);
                    if (count > 0) orderBuckets.insert({i, count});
                }
                permResult.clear();
                recursion(0, kOption, orderBuckets, {});

                for (auto ooperm: permResult) {
                    // create buckets
                    std::unordered_map<std::pair<int, int>, int, pair_hash> buckets; //mapping <k,order> -> count
                    for (int i = 0; i < ooperm.size(); i++) {
                        std::pair<int, int> key(kOption.at(i), ooperm.at(i));
                        auto bucketIt = buckets.find(key);
                        if (bucketIt == buckets.end()) {
                            buckets.insert({key, 1});
                        } else {
                            bucketIt->second++;
                        }
                    }
                    Graph composite(createG());
                    rec(buckets.begin(), buckets, composite, {0, -1, 0});
                }
            }
        }
    }

    // close files
    for (auto& i: ofstreams) {
        i.second->close();
    }
}
