// This was used to convert file with pregraphs to mutliple files with sparse6.
// There is a output file for every combination of order, dangling edge count and girth.

//selects the implemntaion of Graph structure. As of now there is only one.
#include <basic_impl.hpp>
#include <io.hpp>
#include <invariants.hpp>

using namespace ba_graph;

#include <fstream>
#include <iterator>
#include <string>
#include <algorithm>
#include <chrono>

struct pair_hash {
    std::size_t operator () (const std::pair<int, int> &p) const {
        std::string s1 = std::to_string(p.first);
        std::string s2 = std::to_string(p.second);
        std::string s = s1 + s2;
        int c = stoi(s);
        return std::hash<int>{}(c); 
    }
};

std::unordered_map<std::pair<int, int>, std::ofstream *, pair_hash> ofstreams;
std::unordered_map<std::pair<int, int>, int, pair_hash> counters;

void graphhandler(Graph& G, Factory& f, std::string* order) {
    int myGirth = girth(G);
    int danglingEdges = 0;
    for (auto &rotation : G) {
        if (rotation.degree() == 1) {
            danglingEdges++;
        }
    }
    
    std::pair<int, int> key(myGirth, danglingEdges);
    auto i = ofstreams.find(key);
    if (i != ofstreams.end()) {
        *(i->second) << write_sparse6(G);
        auto j = counters.find(key);
        j->second++;
    } else {
        std::stringstream filePath;
        filePath << "../out/order" << *order << "/cubic_o" << *order << "_g" << myGirth << "_k" << danglingEdges;

        std::ofstream* ofs = new std::ofstream();
        ofs->open(filePath.str(), std::ios_base::app);
        *ofs << write_sparse6(G);

        ofstreams[key] = ofs;
        counters[key] = 1;
    }
}

int main() {
    auto t1 = std::chrono::high_resolution_clock::now();
    std::string order = "19";
    std::string basepath = "../pregraphs/output";
    read_pregraph_file<std::string>(basepath.append(order), graphhandler, &order, &static_factory);

    // close files
    for (auto& i: ofstreams) {
        i.second->close();
    }

    // write stats table
    std::stringstream filePath;
    filePath << "../out/order" << order << "/latex_table_o" << order;
    std::ofstream ofs;
    ofs.open(filePath.str(), std::ios_base::trunc);

    int maxK = 0;
    int maxGirth = -1;
    for (auto& i: counters) {
        if (i.first.second > maxK) maxK = i.first.second;
        if (i.first.first > maxGirth) maxGirth = i.first.first;
        if ((i.first.second & 1) != (stoi(order) & 1)) std::cout << "Fuck my life" << "\n";
    }

    ofs << "\\begin{center}\n\\catcode`\\-=12\n\\begin{tabular}{";
    for (int i = 0; i < (maxK / 2) + 2; i++) ofs << "c ";
    ofs << "}\n\\toprule\n\\multirow{2}{*}{Obvod} & \\multicolumn{" << (maxK / 2) + 1 << "}{c}{Počet trčiacich hrán}\\\\\n";
    ofs << "\\cmidrule{2-" << (maxK / 2) + 2 << "}\n";
    for (int i = maxK & 1 ? 1 : 0; i <= maxK; i += 2) ofs << "& " << i;
    ofs << "\\\\\n\\midrule\n";
    for (int i = -1; i <= maxGirth; i++) {
        if (i >= 0 && i <= 2) continue;
        if (i == -1) {
            ofs << "$\\infty$ ";
        } else {
            ofs << i;
        }
        for (int j = maxK & 1 ? 1 : 0; j <= maxK; j += 2) {
            auto c = counters.find(std::pair<int, int>(i, j));
            if (c != counters.end()) {
                ofs << "& " << c->second << " ";
            } else {
                ofs << "& 0 ";
            }
        }
        ofs << "\\\\\n";
    }
    ofs << "\\bottomrule\n\\end{tabular}\n\\end{center}\n";

    ofs.close();
    
    auto t2 = std::chrono::high_resolution_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();

    std::cout << duration << "\n";
    return 0;
}
