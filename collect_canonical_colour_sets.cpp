//selects the implemntaion of Graph structure. As of now there is only one.
#include <basic_impl.hpp>

//#include <impl/basic/include.hpp>
#include <io.hpp>
#include <invariants.hpp>
#include <mrozek.hpp>
//#include <util/cxxopts.hpp>

using namespace ba_graph;

#include <string>
#include <filesystem>
#include <exception>
#include <numeric>
#include <cstring>

struct cba_hash {
    std::size_t operator () (const ColouringBitArray &cba) const {
        std::stringstream binary;
        for (auto i = ColouringBitArray::Index(0, 0); i < cba.size(); i++) {
            if (cba.get(i)) binary << 1;
            else binary << 0;
        }
        return std::hash<std::string>{}(binary.str()); 
    }
};

struct vector_hash {
    std::size_t operator () (const std::vector<uint_fast8_t> &vec) const {
        std::stringstream binary;
        for (auto i : vec) binary << int(i);
        return std::hash<std::string>{}(binary.str()); 
    }
};

struct graph_with_terminal_order {
    std::string graph6;
    std::vector<Number> terminal_order;
};

struct result {
    int order;
    std::vector<graph_with_terminal_order> graphs;
};

std::unordered_map<ColouringBitArray, result, cba_hash> results;
std::unordered_map<std::vector<uint_fast8_t>, result, vector_hash> results_l;
std::vector<int64_t> times;

int counter;

void graphhandler(std::string& line, Graph& g, Factory&, std::pair<int, ColouringMethod>* params) {
    try {
        auto res = get_canonical_colour_set(g, params->second);
        if (std::get<2>(res) != 0) times.push_back(std::get<2>(res));

        auto i = results.find(std::get<1>(res));
        if (i != results.end()) { // if canon already found
            if (i->second.order == params->first) { // if has same order add to results
                i->second.graphs.push_back({ line, std::get<0>(res) });
            } else if (i->second.order > params->first) { // if is smaller delete found and insert new
                i->second.graphs.clear();
                i->second.graphs.push_back({ line, std::get<0>(res) });
                i->second.order = params->first;
            }
        } else { // if first of its kind
            results.insert({std::get<1>(res), { params->first, {{ line, std::get<0>(res) }}}});
        }
    } catch (std::exception& e) {
        counter++;
    }
}

void graphhandler_lukotka(std::string& line, Graph& g, Factory&, std::pair<int, ColouringMethod>* params) {
    try {
        auto res = get_canonical_colour_set_lukotka(g, params->second);
        times.push_back(std::get<2>(res));

        auto i = results_l.find(std::get<1>(res));
        if (i != results_l.end()) { // if canon already found
            if (i->second.order == params->first) { // if has same order add to results
                i->second.graphs.push_back({ line, std::get<0>(res) });
            } else if (i->second.order > params->first) { // if is smaller delete found and insert new
                i->second.graphs.clear();
                i->second.graphs.push_back({ line, std::get<0>(res) });
                i->second.order = params->first;
            }
        } else { // if first of its kind
            results_l.insert({std::get<1>(res), { params->first, {{ line, std::get<0>(res)}}}});
        }
    } catch (std::exception& e) {
        counter++;
    }
}

int main(int argc, char** argv) {
    if (argc != 7) {
        std::cout << "ERROR: too few arguments" << "\n";
        std::cout << "note: all paths are always relative and without '/' at the end" << "\n";
        std::cout << "args format: <path to multipoles> <path where to save result> <number of free ends> <maximal order> <canon to use [l/m]> <colouring method to use [basic/pdc]>" << "\n";
        return 1;
    }
    std::string k = argv[3];
    int max_order = std::stoi(argv[4]);
    bool lukotka;
    if (std::strcmp(argv[5], "l") == 0) {
        lukotka = true;
    } else if (std::strcmp(argv[5], "m") == 0) {
        lukotka = false;
    } else {
        std::cout << "ERROR: wrong format" << "\n";
        std::cout << "args format: <path to multipoles> <path where to save result> <number of free ends> <maximal order> <canon to use [l/m]> <colouring method to use [basic/pdc]>" << "\n";
        return 1;
    }
    ColouringMethod cm;
    if (std::strcmp(argv[6], "basic") == 0) {
        cm = ColouringMethod::basic;
    } else if (std::strcmp(argv[6], "pdc") == 0) {
        cm = ColouringMethod::PDColorizer;
    } else {
        std::cout << "ERROR: wrong format" << "\n";
        std::cout << "args format: <path to multipoles> <path where to save result> <number of free ends> <maximal order> <canon to use [l/m]> <colouring method to use [basic/pdc]>" << "\n";
        return 1;
    }

    std::vector<std::string> paths;
    for (auto& p: std::filesystem::recursive_directory_iterator(argv[1]))
        paths.push_back(p.path());

    for (int order = 0; order <= max_order; order++) {
        std::cout << order << "\n";
        for (auto& path : paths) {
            if (path.find("o" + std::to_string(order) + "_") != std::string::npos &&
                path.find("k" + k) != std::string::npos) {
                std::cout << path << '\n';

                std::pair<int, ColouringMethod> params(order, cm);
                read_graph6_file<std::pair<int, ColouringMethod>>(path, lukotka ? graphhandler_lukotka : graphhandler, &params);
            }
        }
    }

    std::stringstream filePath;
    filePath << argv[2] << "/k" << k << "_summary_" << (cm == ColouringMethod::basic ? "basic" : "pdc") << "_do" << max_order;
    if (lukotka) filePath << "_lukotka";
    std::ofstream ofs;
    ofs.open(filePath.str(), std::ios_base::trunc);

    ofs << "Unique canonical colour sets found: " << (lukotka ? results_l.size() : results.size()) << "\n";
    if (cm == ColouringMethod::PDColorizer) {
        ofs << "Graphs skipped (PDColorizer can not colorize graphs with bridge): " << counter << "\n";
    }
    ofs << "Average canon computation time: " << std::accumulate(times.begin(), times.end(), int64_t(0)) / times.size() << "\n\n";
    if (lukotka) {
        for (auto& i : results_l) {
            for (auto& j : i.first) ofs << int(j);
            ColouringBitArray cba = from_canon(i.first, stoi(k));
            ColourSet colourSet;
            for (auto &colourVector : colouring_bit_array_to_colour_set(cba)) {
                colourSet.insert(internal::colour_vector_to_colour_type(colourVector));
            }
            ofs << "\n" << colourSet << "\n";
        }
    } else {
        for (auto& i : results) {
            for (auto it = ColouringBitArray::Index(0, 0); it < i.first.size(); it++) {
                if (i.first.get(it)) ofs << 1;
                else ofs << 0;
            }
            ColouringBitArray cba = i.first;
            ColourSet colourSet;
            for (auto &colourVector : colouring_bit_array_to_colour_set(cba)) {
                colourSet.insert(internal::colour_vector_to_colour_type(colourVector));
            }
            ofs << "\n" << colourSet << "\n";
        }
    }
    ofs.close();

    std::stringstream filePath2;
    filePath2 << argv[2] << "/k" << k << "_graphs_" << (cm == ColouringMethod::basic ? "basic" : "pdc") << "_do" << max_order;
    if (lukotka) filePath2 << "_lukotka";
    std::ofstream ofs2;
    ofs2.open(filePath2.str(), std::ios_base::trunc);

    if (lukotka) {
        for (auto& i : results_l) {
            std::stringstream canon_ss;
            for (auto& j : i.first) canon_ss << int(j);
            std::string canon_str = canon_ss.str();
            for (auto& it : i.second.graphs) {
                ofs2 << canon_str << " " << it.graph6 << " " << it.terminal_order << " " << i.second.order << "\n";
            }
        }
    } else {
        for (auto& i : results) {
            std::stringstream cba_ss;
            for (auto it = ColouringBitArray::Index(0, 0); it < i.first.size(); it++) {
                if (i.first.get(it)) cba_ss << 1;
                else cba_ss << 0;
            }
            std::string cba_str = cba_ss.str();

            for (auto& it : i.second.graphs) {
                ofs2 << cba_str << " " << it.graph6 << " " << it.terminal_order << " " << i.second.order << "\n";
            }
        }
    }
    ofs2.close();

    std::stringstream filePath3;
    filePath3 << argv[2] << "/k" << k << "_canons_" << (cm == ColouringMethod::basic ? "basic" : "pdc") << "_do" << max_order;
    if (lukotka) filePath3 << "_lukotka";
    std::ofstream ofs3;
    ofs3.open(filePath3.str(), std::ios_base::trunc);
    if (lukotka) {
        for (auto& i : results_l) {
            for (auto& j : i.first) ofs3 << int(j);
            ofs3 << "\n";
        }
    } else {
        for (auto& i : results) {
            ofs3 << i.first << "\n";
        }
    }
    ofs3.close();

    std::cout << "Error count: " << counter << "\n";
}
