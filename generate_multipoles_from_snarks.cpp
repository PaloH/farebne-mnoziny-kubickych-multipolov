// This was used to create all possible snarks from multipoles.

#include <cassert>
#include <cstdio>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <memory>
#include <string>
#include <omp.h>

#include <impl/basic.hpp>
#include <algorithms/cyclic_connectivity.hpp>
#include <algorithms/isomorphism/isomorphism_nauty.hpp>
#include <algorithms/object_collection/object_collection.hpp>
#include <algorithms/object_collection/generation.hpp>
#include <algorithms/paths.hpp>
#include <graphs/stored_graphs.hpp>
#include <invariants/distance.hpp>
#include <io/graph6.hpp>
#include <multipoles.hpp>
#include <operations/copies.hpp>
#include <operations/undo.hpp>
#include <snarks/colouring.hpp>
#include <util/math.hpp>

#include <io/print_nice.hpp>

using namespace ba_graph;

const int progressInterval = 1;
const int terminalShift = 10000;
const std::string outputFile = "generated_multipoles.txt";
const std::string outputFileOnlyGraphs = "generated_multipoles.s6";

const int failedCandidatesPerThread = 1000000;

int K;
int cc;
int order;

int totalGraphs;
int processedGraphs;

class GeneratedMultipole : public GraphWithHash
{
public:
	GeneratedMultipole(const Graph &g) : GraphWithHash(g) {}

	GeneratedMultipole(const std::string &s, bool inflate) : GraphWithHash(s, inflate) {}

	inline int size() const {
		if (minimised)
			throw std::logic_error("minimised, graph is inaccessible");
		else
			return G.order() - G.count(RP::degree(1));
	}
};

/*
 * ================================== creation of multipoles ===================================================
 */

typedef ObjectCollection<GeneratedMultipole, int> MultipoleCollection;

class SplittingInfo {
public:	
	int missingDangling;
	std::set<Number> verticesToDelete;
	std::set<Location> edgesToDelete;
	std::set<Number> verticesToSplit;
	std::set<Location> edgesToSplit;

	SplittingInfo(int missingDangling) : missingDangling(missingDangling) {}

	bool operator<(const SplittingInfo &i2) const
	{
		return std::tuple(verticesToDelete, edgesToDelete, verticesToSplit, edgesToSplit)
				< std::tuple(i2.verticesToDelete, i2.edgesToDelete, i2.verticesToSplit, i2.edgesToSplit);
	}
};

std::ostream & operator<<(std::ostream &out, SplittingInfo info)
{
	out << info.missingDangling << ", "
			<< info.verticesToDelete << ", " << info.edgesToDelete << ", "
			<< info.verticesToSplit << ", " << info.edgesToSplit;
	return out;
}

void add_splitting_info(SplittingInfo &info1, const SplittingInfo &info2, int K)
{
	auto &vd1 = info1.verticesToDelete; auto &vd2 = info2.verticesToDelete;
	vd1.insert(vd2.begin(), vd2.end());
	auto &ed1 = info1.edgesToDelete; auto &ed2 = info2.edgesToDelete;
	ed1.insert(ed2.begin(), ed2.end());
	auto &vs1 = info1.verticesToSplit; auto &vs2 = info2.verticesToSplit;
	vs1.insert(vs2.begin(), vs2.end());
	auto &es1 = info1.edgesToSplit; auto &es2 = info2.edgesToSplit;
	es1.insert(es2.begin(), es2.end());
	info1.missingDangling -= (K - info2.missingDangling);
}

SplittingInfo create_path_splitting_info(Path p, int K)
{
	SplittingInfo info(K);
	for (unsigned i = 0; i < p.vertices().size() - 1; ++i) {
		info.verticesToSplit.insert(p.vertices()[i]);
		info.edgesToDelete.insert(Location(p.vertices()[i], p.vertices()[i+1]));
	}
	info.verticesToSplit.insert(p.vertices().back());

	info.missingDangling -= p.vertices().size() + 2;
	return info;
}

SplittingInfo create_circuit_splitting_info(Circuit c, int K)
{
	SplittingInfo info(K - c.vertices().size());
	for (unsigned i = 0; i < c.vertices().size(); ++i) {
		info.verticesToSplit.insert(c.vertices()[i]);
		info.edgesToDelete.insert(Location(c.vertices()[i], c.vertices()[(i+1) % c.vertices().size()]));
	}
	return info;
}

std::set<SplittingInfo> combine_with_edge_splitting_data(const SplittingInfo &info, const Graph &G, int K)
{
	assert(info.missingDangling % 2 == 0);

	std::set<SplittingInfo> result;
	auto x = G.list(RP::all(), IP::primary(), IT::l());
	auto allEdges = std::set(x.begin(), x.end());
	auto edgeSets = all_subsets(allEdges, info.missingDangling / 2);
	for (auto s : edgeSets) {
		SplittingInfo newInfo(K - info.missingDangling);
		for (auto loc : s)
			newInfo.edgesToSplit.insert(loc);
		add_splitting_info(newInfo, info, K);
		result.insert(newInfo);
	}
	return result;
}

// works for K <= 8 since we only consider at most two paths
// takes care of all cases where a disjoint union of paths is removed and some edges are cut
// e.g. remove a vertex and cut 2 edges to get a 7-pole
std::set<SplittingInfo> collect_splitting_info(const Graph &G, int K)
{
	std::set<SplittingInfo> partialInfos;

	// just split edges
	if (K % 2 == 0)
		partialInfos.insert(SplittingInfo(K));
	// single path combined with split edges
	for (int len1 = 0; len1 <= K - 3; ++len1) {
		int missingDangling = K - len1 - 3;
		if (missingDangling % 2 != 0)
			continue; // cannot be supplemented by cutting edges
		for (auto p : all_paths(G, len1)) {
			SplittingInfo info = create_path_splitting_info(p, K);
			partialInfos.insert(info);
		}		
	}
	// two path components combined with split edges
	for (int len1 = 0; len1 <= K - 3; ++len1) {
		for (int len2 = 0; len2 <= K - 6 - len1; ++len2) {
			int missingDangling = K - len1 - 3 - len2 - 3;
			if (missingDangling % 2 != 0)
				continue; // cannot be supplemented by cutting edges
			for (auto p1 : all_paths(G, len1)) {
				for (auto p2 : all_paths(G, len2)) {
					auto info = create_path_splitting_info(p1, K);
					add_splitting_info(info, create_path_splitting_info(p2, K), K);
					partialInfos.insert(info);
				}
			}
		}
	}
	// one circuit component combined with split edges
	for (int len1 = 3; len1 <= K; ++len1) {
		int missingDangling = K - len1;
		if (missingDangling % 2 != 0)
			continue; // cannot be supplemented by cutting edges
		for (auto c : all_circuits(G, len1)) {
			SplittingInfo info = create_circuit_splitting_info(c, K);
			partialInfos.insert(info);
		}
	}
	// a claw component combined with split edges
	if ((K >= 6) && (K % 2 == 0)) {
		for (auto &rv : G) {
			SplittingInfo info(K - 6);
			info.verticesToDelete.insert(rv.n());
			auto x = G[rv.n()].list(IP::all(), IT::n2());
			info.verticesToSplit.insert(x.begin(), x.end());
			partialInfos.insert(info);
		}
	}

	std::set<SplittingInfo> result;
	for (auto &info : partialInfos) {
		auto r = combine_with_edge_splitting_data(info, G, K);
		result.insert(r.begin(), r.end());
	}

	return result;
}

void reportProgress()
{
	#pragma omp atomic
	processedGraphs++;

	if (processedGraphs % progressInterval == 0) {
		std::cout << "====== processing graph "
				  << processedGraphs << "/" << totalGraphs << " (estimated)"
				  << std::endl << std::flush;
	}
}

void processMultipole(const Graph &B, Multipole mB, bool checkCCObstacles, MultipoleCollection *generated)
{
	mB.flatten();
	auto strB = write_sparse6(B, false);
	auto b = std::make_unique<GeneratedMultipole>(B);
	auto bb = b.get();
	if (generated->is_failed_candidate(bb))
		return;

	// if (is_colourable(B))
	// 	return;

	// if (checkCCObstacles && contains_cc_obstacle(B, mB, cc))
	// 	return;

	// #pragma omp critical
	// {
		generated->add(std::move(b), true);
	// }
}

bool isDegenerate(const Graph &H, SplittingInfo &info)
{
	for (auto loc : info.edgesToSplit) {
		if (!H.contains(loc)) {
			return false;
		}
	}
	for (auto v : info.verticesToSplit) {
		if (!H.contains(v)) {
			return false;
		}
	}
	for (auto loc : info.edgesToSplit) {
		if (info.verticesToSplit.count(loc.n1()) > 0)
			return false;
		if (info.verticesToSplit.count(loc.n2()) > 0)
			return false;
	}

	for (auto loc1 : info.edgesToSplit) {
		for (auto loc2 : info.edgesToSplit) {
			if (loc1.n1() == loc2.n1()) return false;
			if (loc1.n1() == loc2.n2()) return false;
			if (loc1.n2() == loc2.n1()) return false;
			if (loc1.n2() == loc2.n2()) return false;
		}
	}
	return true;
}

bool generateMultipoles(Graph &G, MultipoleCollection *generated)
{
	// int ccG = cyclic_connectivity(G);
	Factory f;
	for (auto info : collect_splitting_info(G, K)) {
		Graph H(copy_other_factory(G, f));
		for (auto loc : info.edgesToDelete)
			deleteE(H, loc, f);
		for (auto v : info.verticesToDelete)
			deleteV(H, v, f);

		if (!isDegenerate(H, info)) continue;

		Multipole m = multipole_by_splitting(H, info.verticesToSplit, info.edgesToSplit, terminalShift, f);
		// check if the splitting set was meaningful
		if (H.count(RP::degree(1)) != K)
			continue;
		if (H.count(RP::degree(2)) > 0)
			continue;
		if (!is_connected(H))
			continue;

		processMultipole(H, m, false, generated);
	}
	return true;
}

/*
 * ================================== main ===================================================
 */

std::string cmdArgv0;

void wrong_usage()
{
	std::cerr << "usage: " << cmdArgv0 << " <dangling edges> <cyclic connectivity> <cubic/snark> <max. order of original graphs>\n";
	exit(1);
}

void graphhandler(std::string& line, Graph& g, Factory&, MultipoleCollection* generated) {
    
    generateMultipoles(g, generated);
	
}

int main(int argc, char **argv)
{   
	if (argc != 5) {
        std::cout << "ERROR: too few arguments" << "\n";
        std::cout << "args format: <path to snarks> <output path> <order> <K>" << "\n";
        return 1;
    }
    int order = std::stoi(argv[3]);
	K = std::stoi(argv[4]);

    std::vector<std::string> paths;
    for (auto& p: std::filesystem::recursive_directory_iterator(argv[1]))
        paths.push_back(p.path());

	std::stringstream filePath;
	filePath << argv[2] << "/multipoles_from_snarks_o" << order << "_k" << K;
	std::string outPath = filePath.str();

	MultipoleCollection generated("", 
			MultipoleCollection::ContainsAlgorithm::HASH_EQUALS,
			false, 100000000, false);
		
	for (auto& path : paths) {
		if (path.find("o" + std::to_string(order) + "_") != std::string::npos) {
			std::cout << path << '\n';

			read_graph6_file<MultipoleCollection>(path, graphhandler, &generated);
		}
	}

	auto multipoles = generated.get_all();

	std::cout << "writing " << multipoles.size() << " multipoles" << std::endl;
	std::ofstream ofs;
	ofs.open(outPath, std::ios_base::app);
	for (auto &gg : multipoles) ofs << gg->serialise() << std::endl;
	ofs.close();
}
