// This file was used for verifying whether Col(P) >= Col(A_i)

//selects the implemntaion of Graph structure. As of now there is only one.
#include <basic_impl.hpp>

//#include <impl/basic/include.hpp>
#include <io.hpp>
#include <invariants.hpp>
#include <mrozek.hpp>
//#include <util/cxxopts.hpp>

using namespace ba_graph;

#include <string>
#include <sstream>
#include <fstream>
#include <memory>

struct vector_hash {
    std::size_t operator () (const std::vector<int> &vec) const {
        std::stringstream binary;
        for (auto i : vec) binary << i;
        return std::hash<std::string>{}(binary.str()); 
    }
};

ColouringBitArray parse_cba(std::string cba_str) {
    ColouringBitArray cba(cba_str.size(), false);
    for(int i = 0; i < cba_str.size(); i++) {
        if (cba_str[i] == '1') cba.set(ColouringBitArray::Index::to_index(i), true);
    }
    return cba;
}

ColourSet colourTypeToColourVectors(ColourVector cv) {
    std::vector<std::vector<std::vector<int>>> transformationMaps = {
        {{0,1}, {1,0}},
        {{0,2}, {2,0}},
        {{1,2}, {2,1}},
        {{0,1}, {1,2}, {2,0}},
        {{0,2}, {2,1}, {1,0}}
    };

    ColourSet cs;
    cs.insert(cv);
    for (auto map : transformationMaps) {
        ColourVector newVector = cv;
        for (int i = 0; i < newVector.size(); i++) {
            for (auto mapping: map) {
                if (mapping[0] == cv.at(i))
                    newVector.at(i) = mapping[1];
            }
        }
        cs.insert(newVector);
    }

    return cs;
}

ColourSet colourSetToFullColourSet(ColourSet cs) {
    ColourSet fullColourSet;
    for (ColourVector cv: cs) {
        ColourSet part = colourTypeToColourVectors(cv);
        fullColourSet.insert(part.begin(), part.end());
    }
    return fullColourSet;
}

int main() {
    std::vector<ColourVector> allVectors = {
        {0,1,2,2,1,0},
        {0,1,2,1,2,0},
        {0,1,1,2,2,0},
        {0,1,2,2,0,1},
        {0,1,2,1,0,2},
        {0,1,1,2,0,2},
        {0,1,2,0,2,1},
        {0,1,2,0,1,2},
        {0,1,1,0,2,2},
        {0,1,0,2,2,1},
        {0,1,0,2,1,2},
        {0,1,0,1,2,2},
        {0,0,1,2,2,1},
        {0,0,1,2,1,2},
        {0,0,1,1,2,2},
        {0,0,0,0,1,1},
        {0,0,0,1,0,1},
        {0,0,0,1,1,0},
        {0,0,1,0,0,1},
        {0,0,1,0,1,0},
        {0,0,1,1,0,0},
        {0,1,0,0,0,1},
        {0,1,0,0,1,0},
        {0,1,0,1,0,0},
        {0,1,1,0,0,0},
        {0,1,1,1,1,0},
        {0,1,1,1,0,1},
        {0,1,1,0,1,1},
        {0,1,0,1,1,1},
        {0,0,1,1,1,1},
        {0,0,0,0,0,0},
    };
    std::vector<int> sets = {
        1112539137, 219, 995328,
        1782, 983094, 983277, 1186463953, 259394321, 1186467169,
        // 15275, 986027, 1003880, 1003955, 1005470,
        // 107940770, 108429655, 108429708, 108431226, 108441953,
        // 108442042, 108454116, 108456866, 259391628,
        // 259394506, 259394506, 259415138, 1112539354, 1112542108, 1112554410, 1113522412, 1113525162, 1113543090, 1113545460,
        // 1113545730, 1186479009, 1186490916, 112984273, 127372309,
        7846403, 113014676, 251396177, 112987505, 127384611, 
        // 258968226, 117291537, 16744821
    };

    std::vector<std::vector<int>> indexPerms;
    std::vector<int> permutation = {0,1,2,3,4,5};
    do {
        std::vector<int> copy = permutation;
        indexPerms.push_back(copy);
    } while (std::next_permutation(permutation.begin(), permutation.end()));

    std::cout << indexPerms.size() << "\n";

    std::vector<ColourSet> colourSets;    
    for (auto set : sets) {
        ColourSet cs;
        for (int i = 0; i < 32; i++) {
            unsigned mask = 1 << i;
            if (set & mask) {
                cs.insert(allVectors.at(i));
            }
        }
        colourSets.push_back(cs);
    }

    std::ifstream infile("../out/k6_canons_basic_do28");
    std::string line;
    while (std::getline(infile, line)) {
        ColouringBitArray cba = parse_cba(line);
        ColourSet cs = colouring_bit_array_to_colour_set(cba);
        std::unordered_set<ColourVector, vector_hash> hashedCs;
        hashedCs.insert(cs.begin(), cs.end());
        bool ok = false;
        for (ColourSet csAi : colourSets) {
            for (std::vector<int> perm : indexPerms) {
                int found = 0;
                for (ColourVector cv : csAi) {
                    ColourVector permutedCv(6);
                    for (int i = 0; i < permutedCv.size(); i++) {
                        permutedCv.at(i) = cv.at(perm.at(i));
                    }
                    auto it = hashedCs.find(permutedCv);
                    if (it == hashedCs.end()) break;
                    found++;
                }
                if (found == csAi.size()) {
                    ok = true;
                    goto next_canon;
                }
            }
        }
        next_canon:;
        if (!ok) {
            std::cout << "not matched: " << line << "\n";
        }
    }
}