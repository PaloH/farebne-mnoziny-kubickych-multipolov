CFLAGS = -std=c++17 -fconcepts -I ./ba-graph/include/ 
DBGFLAGS = -g -O0 -pedantic -Wall -Wextra -DBA_GRAPH_DEBUG
FASTFLAGS = -O3 -pedantic -Wall -Wextra
COMPILE_DBG = $(CXX) $(CFLAGS) $(DBGFLAGS)
COMPILE_FAST = $(CXX) $(CFLAGS) $(FASTFLAGS)


all: test


compile: compile_test


test: compile_test
	./test.out
compile_test:
	$(COMPILE_DBG) main.cpp -o test.out

speed: compile_speed
	./speed.out
compile_speed:
	$(COMPILE_FAST) main.cpp -o speed.out

convert_pregraphs_to_sparse6: compile_convert_pregraphs_to_sparse6
	./convert_pregraphs_to_sparse6.out
compile_convert_pregraphs_to_sparse6:
	$(COMPILE_FAST) convert_pregraphs_to_sparse6.cpp -o convert_pregraphs_to_sparse6.out

get_all_colour_types: compile_get_all_colour_types
	./get_all_colour_types.out
compile_get_all_colour_types:
	$(COMPILE_DBG) get_all_colour_types.cpp -o get_all_colour_types.out

cccs: compile_collect_canonical_colour_sets
compile_collect_canonical_colour_sets:
	$(COMPILE_FAST) collect_canonical_colour_sets.cpp -o cccs_with_allfalse.out

ccs: compile_connect_colour_sets
compile_connect_colour_sets:
	$(COMPILE_FAST) -DBA_GRAPH_DEBUG connect_colour_sets.cpp -o ccs.out

cccsmt: compile_collect_canonical_colour_sets_mt
	./collect_canonical_colour_sets_mt.out
compile_collect_canonical_colour_sets_mt:
	$(COMPILE_FAST) -pthread collect_canonical_colour_sets_mt.cpp -o collect_canonical_colour_sets_mt.out

create_multi_component_graphs: compile_create_multi_component_graphs
	./create_multi_component_graphs.out
compile_create_multi_component_graphs:
	$(COMPILE_FAST) create_multi_component_graphs.cpp -o create_multi_component_graphs.out

generate_multipoles_from_snarks: compile_generate_multipoles_from_snarks
generate_multipoles_from_snarks:
	$(COMPILE_FAST) -I ../../other/nauty26r12/ generate_multipoles_from_snarks.cpp -o generate_multipoles_from_snarks.out

checkIfWeFoundNew: compile_checkIfWeFoundNew
	./checkIfWeFoundNew.out
compile_checkIfWeFoundNew:
	$(COMPILE_FAST) checkIfWeFoundNew.cpp -o checkIfWeFoundNew.out

count_new_on_order: compile_count_new_on_order
compile_count_new_on_order:
	$(COMPILE_FAST) countNewOnOrder.cpp -o countNewOnOrder.out

check: compile_check
	./checkColPgeColAi.out
compile_check:
	$(COMPILE_FAST) checkColPgeColAi.cpp -o checkColPgeColAi.out

clean:
	rm -rf *.out


.PHONY: clean all compile test compile_test
